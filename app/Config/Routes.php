<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

$routes->get('/', 'Home::login');
$routes->get('/inicio', "Home::dashboard");
$routes->get('/perfil/(:num)','Perfil::perfil/$1');
$routes->post('/signin', "Login::autenticar");
$routes->get('/logout', "Login::logout");
//Rutas para la busqueda de la Division Politico territorial de Venezuela
$routes->post('/municipios', "Caso::obtenerMunicipio");
$routes->post('/parroquias', "Caso::obtenerParroquia");
//Rutas para la generacion de casos por los Usuarios
$routes->get('/nuevoCaso',"Caso::index");
$routes->post('/registrarCaso', 'Caso::nuevoCaso');
//Rutas para la consulta de los casos en el pool de casos
$routes->get('/verCaso/(:num)', "Caso::vercaso/$1");
$routes->get('/casosActivos',"Caso::listadoCasosActivos");
$routes->get('/historicoCasos', "Caso::historicoCasos");
//Rutas para los reportes de los usuarios
$routes->get('/reporte/(:num)', "Reporte::tipoReporte/$1");
$routes->post('/reporte', "Reporte::generarReporte");
//Rutas para el administrador del sistema
$routes->get('/adminUsers',"Administrador::adminUsers");
$routes->get('/adminRoles',"Administrador::adminRoles"); 
$routes->post('/addNewUser', "Administrador::addUsuarios");
$routes->post('/getUser', "Administrador::obtenerUsuario");
$routes->post('/editUser', "Administrador::editarUsuario");
$routes->post('/actualizarTablaUsuarios', "Administrador::actualizaTabla");
//Rutas para los seguimientos
$routes->post('/addSeguimiento', "Seguimiento::addSeguimiento");
$routes->post('/gettl', "Seguimiento::obtenerTL");
//Rutas para realizar el cambio de estatus en los casos
$routes->post('/cambiarEstatus', "Estatus::cambioEstatus");
//Rutas generales de la aplicacion
$routes->get('/403', "Home::forbidden");
$routes->get('/404', "Home::notFound");

//Rutas para el endpoint de datos que vamos a generar
$routes->get('/authToken','Api::authToken');
$routes->get('/dataExcel', 'Api::excelReport');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
