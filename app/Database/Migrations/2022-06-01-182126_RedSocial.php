<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class RedSocial extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idrrss'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'rsnom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
        ]);
        $this->forge->addKey('idrrss', true);
        $this->forge->createTable('red_social');
    }

    public function down()
    {
        $this->forge->dropTable('red_social');
    }
}
