<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UsuarioOperador extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idusuopr'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'usuopnom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
            'usuopape'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
            'usuoppass'       => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
            'usuopemail'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
            'idrol'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
        ]);
        $this->forge->addKey('idusuopr', true);
        $this->forge->addForeignKey('idrol', 'roles', 'idrol', true, true);
        $this->forge->createTable('usuario_operador');
    }

    public function down()
    {
        $this->forge->dropTable('usuario_operador');
    }
}
