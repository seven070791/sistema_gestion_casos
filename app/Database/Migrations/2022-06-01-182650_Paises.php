<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Paises extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'paisid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'paisnom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
        ]);
        $this->forge->addKey('paisid', true);
        $this->forge->createTable('paises');
    }

    public function down()
    {
        $this->forge->dropTable('paises');
    }
}
