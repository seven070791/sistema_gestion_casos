<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TipoPropIntelec extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idtippropint'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'tippropnom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
        ]);
        $this->forge->addKey('idtippropint', true);
        $this->forge->createTable('tipo_prop_intelec');
        //$this->forge->addForeignKey('idtiprequsu', 'tipo_req_usu', 'idtiprequsu', true, true);
    }

    public function down()
    {
        $this->forge->dropTable('tipo_prop_intelec');
    }
}
