<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class SeguimientoCaso extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idsegcas'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'idcaso'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
            'idestllam'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
            'segcoment'       => [
                'type'       => 'VARCHAR',
                'constraint' => '512',
            ],
            'segfec'       => [
                'type'       => 'DATE',
            ],
            'idusuopr'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
        ]);
        $this->forge->addKey('idsegcas', true);
        $this->forge->addForeignKey('idcaso', 'casos', 'idcaso', true, true);
        $this->forge->addForeignKey('idestllam', 'estatus_llamadas', 'idestllam', true, true);
        $this->forge->addForeignKey('idusuopr', "usuario_operador", "idusuopr", true, true);
        $this->forge->createTable('seguimiento_caso');
    }

    public function down()
    {
        $this->forge->dropTable('seguimiento_caso');
    }
}
