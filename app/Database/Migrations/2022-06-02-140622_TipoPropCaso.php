<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TipoPropCaso extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idtipopropcaso'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'idtippropint'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
            'idcaso'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
        ]);
        $this->forge->addKey('idtipopropcaso', true);
        $this->forge->addForeignKey('idcaso', 'casos', 'idcaso', true, true);
        $this->forge->addForeignKey('idtippropint', 'tipo_prop_intelec', 'idtippropint', true, true);
        $this->forge->createTable('tipo_prop_caso');
    }

    public function down()
    {
        $this->forge->dropTable('tipo_prop_caso');
    }
}
