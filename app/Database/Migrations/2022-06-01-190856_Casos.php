<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Casos extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idcaso'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'casofec'       => [
                'type'       => 'DATE',
            ],
            'casoced'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
            'casonom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
            'casoape'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
            'casotel'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
            'casonumsol'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
                
            ],
            'idest'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
            'idrrss'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
            'idusuopr'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
            'estadoid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
            'municipioid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
            'parroquiaid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
            'casociudad'       => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
            'ofiid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
            'casodesc'          => [
                'type'           => 'VARCHAR',
                'constraint'     => '4096',
            ],
        ]);
        $this->forge->addKey('idcaso', true);
        $this->forge->addForeignKey('idest', 'estatus', 'idest', true, true);
        $this->forge->addForeignKey('idrrss', 'red_social', 'idrrss', true, true);
        $this->forge->addForeignKey('idusuopr', 'usuario_operador', 'idusuopr', true, true);
        $this->forge->addForeignKey('estadoid', 'estados', 'estadoid', true, true);
        $this->forge->addForeignKey('municipioid', 'municipio', 'municipioid', true, true);
        $this->forge->addForeignKey('parroquiaid', 'parroquias', 'parroquiaid', true, true);
        $this->forge->createTable('casos');
    }

    public function down()
    {
        $this->forge->dropTable('casos');
    }
}
