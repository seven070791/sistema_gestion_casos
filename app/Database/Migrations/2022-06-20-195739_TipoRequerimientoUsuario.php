<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TipoRequerimientoUsuario extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idtiporeqcaso'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'idtiprequsu'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'idcaso'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
        ]);
        $this->forge->addKey('idtiporeqcaso', true);
        $this->forge->createTable('tipo_req_usu_caso');
    }

    public function down()
    {
        $this->forge->dropTable('tipo_req_usu_caso');
    }
}
