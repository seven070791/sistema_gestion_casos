<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TipoRequerimiento extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idtiprequsu'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'tiprequsunom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '512',
            ],
            'tipreqpi' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ]
        ]);
        $this->forge->addKey('idtiprequsu', true);
        $this->forge->createTable('tipo_req_usu');
    }

    public function down()
    {
        $this->forge->dropTable('tipo_req_usu');
    }
}
