<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class EstatusLlamadas extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idestllam'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'estllamnom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
        ]);
        $this->forge->addKey('idestllam', true);
        $this->forge->createTable('estatus_llamadas');
    }

    public function down()
    {
        $this->forge->dropTable('estatus_llamadas');
    }
}
