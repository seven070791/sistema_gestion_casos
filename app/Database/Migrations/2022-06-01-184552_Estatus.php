<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Estatus extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idest'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'estnom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
        ]);
        $this->forge->addKey('idest', true);
        $this->forge->createTable('estatus');
    }

    public function down()
    {
        $this->forge->dropTable('estatus');
    }
}
