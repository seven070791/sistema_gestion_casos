<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class DireccionCorrespondiente extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'iddircor'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'dircornom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
        ]);
        $this->forge->addKey('iddircor', true);
        $this->forge->createTable('direccion_correspondiente');
    }

    public function down()
    {
        $this->forge->dropTable('direccion_correspondiente');
    }
}
