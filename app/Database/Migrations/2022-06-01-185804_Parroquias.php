<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Parroquias extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'parroquiaid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'parroquianom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
            'municipioid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
        ]);
        $this->forge->addKey('parroquiaid', true);
        $this->forge->addForeignKey('municipioid', 'municipio', 'municipioid', true, true);
        $this->forge->createTable('parroquias');
    }

    public function down()
    {
        $this->forge->dropTable('parroquias');
    }
}
