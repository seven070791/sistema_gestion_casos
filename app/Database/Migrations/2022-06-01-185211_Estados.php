<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Estados extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'estadoid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'estadonom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
            'paisid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
        ]);
        $this->forge->addKey('estadoid', true);
        $this->forge->addForeignKey('paisid', 'paises', 'paisid', TRUE, TRUE);
        $this->forge->createTable('estados');
    }

    public function down()
    {
        $this->forge->dropTable('estados');
    }
}
