<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Oficinas extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'idofi'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'ofinom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
        ]);
        $this->forge->addKey('idofi', TRUE);
        $this->forge->createTable('oficinas');
    }

    public function down()
    {
        $this->forge->dropTable('oficinas');
    }
}
