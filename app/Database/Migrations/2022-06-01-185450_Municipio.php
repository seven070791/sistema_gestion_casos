<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Municipio extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'municipioid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'municipionom'       => [
                'type'       => 'VARCHAR',
                'constraint' => '48',
            ],
            'estadoid'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => false,
            ],
        ]);
        $this->forge->addKey('municipioid', true);
        $this->forge->addForeignKey('estadoid', 'estados', 'estadoid', true, true);
        $this->forge->createTable('municipio');
    }

    public function down()
    {
        $this->forge->dropTable('municipio');
    }
}
