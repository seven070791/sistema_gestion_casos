<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Pais extends Seeder
{
    public function run()
    {
        $data = [
            'paisid' => 1,
            'paisnom' => 'Venezuela',
        ];

        $this->db->table('paises')->insert($data);
        
    }
}
