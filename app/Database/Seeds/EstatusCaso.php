<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class EstatusCaso extends Seeder
{
    public function run()
    {
        $data = [
            [
                'idest'   => 1,
                'estnom'  => 'Abierto'
            ],
            [
                'idest'  => 2,
                'estnom' => 'En Seguimiento'
            ],
            [
                'idest'  => 3,
                'estnom' => 'Cerrado'
            ],
            [
                'idest'  => 4,
                'estnom' => 'Reabierto'
            ],
        ];
        $this->db->table('estatus')->insertBatch($data);
    }
}
