<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RRSS extends Seeder
{
    public function run()
    {
        $data = [
            [
                'idrrss' => 1,
                'rsnom'  => 'Facebook'
            ],
            [
                'idrrss' => 2,
                'rsnom'  => 'Twitter'
            ],
            [
                'idrrss' => 3,
                'rsnom'  => 'Instagram'
            ],
            [
                'idrrss' => 4,
                'rsnom'  => 'Correo Electronico'
            ],
            [
                'idrrss' => 6,
                'rsnom'  => 'Llamada telefonica'
            ],
        ];
        $this->db->table('red_social')->insertBatch($data);
    }
}
