<?php

namespace App\Database\Seeds;
use CodeIgniter\Database\Seeder;


class CasesSample extends Seeder
{
    public function run()
    {
        $year       = ['2023','2022','2021','2020'];
        $faker      = \Faker\Factory::create();   
        
        //making data samples
        $cases = array();
        for($i = 1; $i <= 10000; $i++)
        {
            
            $estadoid = rand(1,24);
            $municipioid = $this->db->table('municipio')->getWhere(['estadoid' => $estadoid], 1)->getResultArray()[0]['municipioid'];
            $parroquiaid = $this->db->table('parroquias')->getWhere(['municipioid' => $municipioid], 1)->getResultArray()[0]['parroquiaid'];
            //generamos los casos en funcion de los estados y los municipios
            $cases[] = array(
                'idcaso' => $i,
                'casofec' => $year[rand(0,3)].'-'.strval(rand(1,12)).'-'.strval(rand(1,28)),
                'casoced' => rand(1000000, 30000000),
                'casonom' => $faker->name(),
                'casoape' => $faker->lastName(),
                'casotel' => $faker->phoneNumber(),
                'idest' => rand(1,4),
                'idrrss' => rand(1,4),
                'idusuopr' => 1,
                'estadoid' => $estadoid,
                'municipioid' => $municipioid,
                'parroquiaid' => $parroquiaid,
                'casociudad' => $faker->city(),
                'ofiid' => rand(1,2),
                'casodesc' => $faker->text(1024),
                'casonumsol' => strval(rand(2000,2023)).'-'.strval(rand(1000,15000))
            );
            //generamos ahora los seguimientos
            $seguimiento[] = array(
                "idcaso" => $i,
                "idestllam" => 1, //Seteamos el valor en 1, que es el estatus por llamar
                "segcoment" => "Caso creado el dia ".date('Y-m-d'),
                "segfec" => date('Y-m-d'),
                "idusuopr" => 1
            );
            $reqUser = array();
            for($j = 1; $j >= 6; $j++){
                $reqUser[] = array(
                    'idcaso' => $i,
                    'idtiprequsu' => rand(1,60),
                );
            }
            $tipoPropIntelec[] = array(
                'idtippropint' => rand(1,3),
                'idcaso'       => $i   
            );
        }

        //Insertamos en la base de datos
        $this->db->table('sgc_casos')->insertBatch($cases);
        $this->db->table('sgc_tipo_prop_caso')->insertBatch($tipoPropIntelec);
        $this->db->table('sgc_seguimiento_caso')->insertBatch($seguimiento);
        $this->db->table('sgc_tipo_req_usu_caso')->insertBatch($reqUser);
    }
}
