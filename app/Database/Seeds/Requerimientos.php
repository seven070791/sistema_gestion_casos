<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Requerimientos extends Seeder
{
    public function run()
    {
        $data = [
            [
                'idtiprequsu' => 1,
                'tiprequsunom'   => 'Clase 1: Productos químicos para la industria, la ciencia y la fotografía, así como para la agricultura, la horticultura y la silvicultura',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 2,
                'tiprequsunom'   => 'Clase 2: Pinturas, barnices, lacas',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 3,
                'tiprequsunom'   => 'Clase 3: Productos cosméticos y preparaciones de tocador no medicinales',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 4,
                'tiprequsunom'   => 'Clase 4: Aceites y grasas para uso industrial, ceras; lubricantes',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 5,
                'tiprequsunom'   => 'Clase 5: Productos farmacéuticos, preparaciones para uso médico y veterinario',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 6,
                'tiprequsunom'   => 'Clase 6: Metales comunes y sus aleaciones, menas',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 7,
                'tiprequsunom'   => 'Clase 7: Máquinas, máquinas herramientas y herramientas mecánicas',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 8,
                'tiprequsunom'   => 'Clase 8: Herramientas e instrumentos de mano que funcionan manualmente',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 9,
                'tiprequsunom'   => 'Clase 9: Aparatos e instrumentos científicos, de investigación',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 10,
                'tiprequsunom'   => 'Clase 10: Aparatos e instrumentos quirúrgicos, médicos, odontológicos y veterinarios',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 11,
                'tiprequsunom'   => 'Clase 11: Aparatos e instalaciones de alumbrado, calefacción, enfriamiento',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 12,
                'tiprequsunom'   => 'Clase 12: Vehículos',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 13,
                'tiprequsunom'   => 'Clase 13: Armas de fuego',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 14,
                'tiprequsunom'   => 'Clase 14: Metales preciosos y sus aleaciones',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 15,
                'tiprequsunom'   => 'Clase 15: Instrumentos musicales',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 16,
                'tiprequsunom'   => 'Clase 16: Papel y cartón',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 17,
                'tiprequsunom'   => 'Clase 17: Caucho, gutapercha, goma, amianto y mica en bruto o semielaborados, así como sucedáneos de estos materiales',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 18,
                'tiprequsunom'   => 'Clase 18: Cuero y cuero de imitación',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 19,
                'tiprequsunom'   => 'Clase 19: Materiales de construcción no metálicos',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 20,
                'tiprequsunom'   => 'Clase 20: Muebles, espejos, marcos',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 21,
                'tiprequsunom'   => 'Clase 21: Utensilios y recipientes para uso doméstico y culinario',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 22,
                'tiprequsunom'   => 'Clase 22: Cuerdas y cordeles',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 23,
                'tiprequsunom'   => 'Clase 23: Muebles, espejos, marcos',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 24,
                'tiprequsunom'   => 'Clase 24: Hilos e hilados para uso textil.',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 25,
                'tiprequsunom'   => 'Clase 25: Tejidos y sus sucedáneos',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 26,
                'tiprequsunom'   => 'Clase 26: Prendas de vestir, calzado, artículos de sombrerería.',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 27,
                'tiprequsunom'   => 'Clase 27: Encajes, cordones y bordados, así como cintas y lazos de mercería',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 28,
                'tiprequsunom'   => 'Clase 28: Alfombras, felpudos, esteras y esterillas, linóleo y otros revestimientos de suelos',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 29,
                'tiprequsunom'   => 'Clase 29: Juegos y juguetes',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 30,
                'tiprequsunom'   => 'Clase 30: Café, té, cacao y sus sucedáneos',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 31,
                'tiprequsunom'   => 'Clase 31: Productos agrícolas, acuícolas, hortícolas y forestales en bruto y sin procesar',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 32,
                'tiprequsunom'   => 'Clase 32: Cervezas; bebidas sin alcohol; aguas minerales y carbonatadas',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 33,
                'tiprequsunom'   => 'Clase 33: Bebidas alcohólicas, excepto cervezas',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 34,
                'tiprequsunom'   => 'Clase 34: Tabaco y sucedáneos del tabaco',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 35,
                'tiprequsunom'   => 'Clase 35: Publicidad; gestión, organización y administración de negocios comerciales; trabajos de oficina',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 36,
                'tiprequsunom'   => 'Clase 36: Servicios financieros, monetarios y bancarios',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 37,
                'tiprequsunom'   => 'Clase 37: Servicios de construcción',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 38,
                'tiprequsunom'   => 'Clase 38: Servicios de telecomunicaciones',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 39,
                'tiprequsunom'   => 'Clase 39: Transporte; embalaje y almacenamiento de mercancías; organización de viajes.',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 40,
                'tiprequsunom'   => 'Clase 40: Tratamiento de materiales; reciclaje de residuos y desechos',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 41,
                'tiprequsunom'   => 'Clase 41: Educación; formación',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 42,
                'tiprequsunom'   => 'Clase 42: Servicios científicos y tecnológicos, así como servicios de investigación y diseño conexos',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 43,
                'tiprequsunom'   => 'Clase 43: Servicios de restauración (alimentación); hospedaje temporal.',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 44,
                'tiprequsunom'   => 'Clase 44: Servicios médicos; servicios veterinarios',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 45,
                'tiprequsunom'   => 'Clase 45: Servicios jurídicos; servicios de seguridad para la protección física de bienes materiales y personas',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 46,
                'tiprequsunom'   => 'Clase 46: Es la Marca que tiene por objeto distinguir una empresa, negocio, explotación o establecimiento mercantil, industrial agrícola o minero.',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 47,
                'tiprequsunom'   => 'Clase 47: Es la marca que consiste, en una palabra , frase o leyenda utilizada por un industrial, comerciante o agricultor, como complemento de una marca o denominación comercial.',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 48,
                'tiprequsunom'   => 'Clase 48: Son signos distintivos que identifican un producto como originario del territorio de un país, una región o localidad de ese territorio',
                'tipreqpi'   => 1
            ],
            [
                'idtiprequsu' => 49,
                'tiprequsunom'   => 'Invencion',
                'tipreqpi'   => 2
            ],
            [
                'idtiprequsu' => 50,
                'tiprequsunom'   => 'Dibujo Industrial',
                'tipreqpi'   => 2
            ],
            [
                'idtiprequsu' => 51,
                'tiprequsunom'   => 'Modelo Industrial',
                'tipreqpi'   => 2
            ],
            [
                'idtiprequsu' => 52,
                'tiprequsunom'   => 'Diseño Industrial',
                'tipreqpi'   => 2
            ],
            [
                'idtiprequsu' => 53,
                'tiprequsunom'   => 'Actos y Contratos',
                'tipreqpi'   => 3
            ],
            [
                'idtiprequsu' => 54,
                'tiprequsunom'   => 'Programas de Computación',
                'tipreqpi'   => 3
            ],
            [
                'idtiprequsu' => 55,
                'tiprequsunom'   => 'Audiovisuales y Radiofonicos',
                'tipreqpi'   => 3
            ],
            [
                'idtiprequsu' => 56,
                'tiprequsunom'   => 'Arte Visual',
                'tipreqpi'   => 3
            ],
            [
                'idtiprequsu' => 57,
                'tiprequsunom'   => 'Obras Literarias',
                'tipreqpi'   => 3
            ],
            [
                'idtiprequsu' => 58,
                'tiprequsunom'   => 'Obras Musicales',
                'tipreqpi'   => 3
            ],
            [
                'idtiprequsu' => 59,
                'tiprequsunom'   => 'Obras Escenicas',
                'tipreqpi'   => 3
            ],
            [
                'idtiprequsu' => 60,
                'tiprequsunom'   => 'Obras Produccion Fonografica',
                'tipreqpi'   => 3
            ],
        ];
        $this->db->table('tipo_req_usu')->insertBatch($data);
    }
}
