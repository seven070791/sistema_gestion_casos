<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Roles extends Seeder
{
    public function run()
    {
        $data = [
            [
                'idrol' => 1,
                'rolnom' => 'Administrador'
            ],
            [
                'idrol' => 2,
                'rolnom' => 'Supervisor'
            ],
            [
                'idrol' => 3,
                'rolnom' => 'Operador'
            ],
        ];
        $this->db->table('roles')->insertBatch($data);
    }
}
