<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Usuarios extends Seeder
{
    public function run()
    {
        $data = [
            'idusuopr' => 1,
            'usuopnom' => 'Bryan',
            'usuopape' => 'Useche',
            'usuoppass'=> '$2y$10$mhAzAZAY5tLHwXxCStxjjuJ1ZRZRWlmAjn1AMsNBVTNO6pMdef28i',
            'usuopemail'=> 'bryan.useche@sapi.gob.ve',
            'idrol'     => 1
        ];
        $this->db->table('usuario_operador')->insert($data);
    }
}
