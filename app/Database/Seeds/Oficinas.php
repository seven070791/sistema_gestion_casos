<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Oficinas extends Seeder
{
    public function run()
    {
        $data = [
            [
                'idofi' => 1,
                'ofinom' => 'Sala Situacional',
            ],
            [
                'idofi' => 2,
                'ofinom' => 'Coordinacion Regional',
            ],
        ];

        $this->db->table('oficinas')->insertBatch($data);
    }
}
