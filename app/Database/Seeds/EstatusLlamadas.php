<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class EstatusLlamadas extends Seeder
{
    public function run()
    {
        $data = [
            [
                'idestllam'   => 1,
                'estllamnom'  => 'Por Llamar'
            ],
            [
                'idestllam'  => 2,
                'estllamnom' => 'Atendido'
            ],
            [
                'idestllam'  => 3,
                'estllamnom' => 'No Contesta'
            ],
        ];
        $this->db->table('estatus_llamadas')->insertBatch($data);
    }
}
