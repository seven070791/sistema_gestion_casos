<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class TipoProcIntelec extends Seeder
{
    public function run()
    {
        $data = [
            [
                'idtippropint' => 1,
                'tippropnom'   => 'Marcas'
            ],
            [
                'idtippropint' => 2,
                'tippropnom'   => 'Patentes'
            ],
            [
                'idtippropint' => 3,
                'tippropnom'   => 'Derecho de Autor'
            ],
            [
                'idtippropint' => 4,
                'tippropnom'   => 'Indicaciones Geograficas'
            ],
        ];
        $this->db->table('tipo_prop_intelec')->insertBatch($data);
    }
}
