<?php namespace App\Controllers;

use App\Models\Usuarios;
use App\Models\Roles;
use CodeIgniter\API\ResponseTrait;


class Administrador extends BaseController{

	use ResponseTrait;

	//Metodo que carga la vista de los usuarios registrados en el sistema
	public function adminUsers(){
		$model = new Usuarios();
		$rolModel = new Roles();
		$headings = array('ID', "Nombre", "Apellido","Rol de Usuario","Correo Electronico","Acciones");
		$rows = array();
		if($this->session->get('userrol') == 1){
			$query = $model->getAllUsers();
			if(isset($query) > 0){
				foreach($query->getResult() as $row){
					$rows[] = array($row->idusuopr, $row->usuopnom, $row->usuopape,$row->rolnom,$row->usuopemail, '<a class="editar" href="#" id="'.$row->idusuopr.'"> Detalles</a>');
				}
			}
			else{
				$rows[] = array('<td colspan="6">Sin Registros</td>','','','','',"");
			}
			unset($query);
			//Preguntamos por los roles de los usuarios
			$query = $rolModel->getRoles();
			//Generamos los option para los formularios
			$opt = '';
			if(isset($query)){
				foreach ($query->getResult() as $row) {
					$opt .= '<option value="'.$row->idrol.'">'.$row->rolnom.'</option>';
				}
			}
			$data["roles"] = $opt;
			$data["tabla"] = $this->generarTabla($headings,$rows);
			return $this->loadTemplate('administrador/usuarios',$data);
		}
		else{
			return redirect()->to('/403');
		}
	}

	//Metodo para añadir usuarios

	public function addUsuarios(){
		$model = new Usuarios();
		if($this->request->isAJAX() and $this->session->get('userrol') == 1){
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))),TRUE);
			$query = $model->addUsuario(array(
				"usuopnom" => $datos["username"],
				"usuopape" => $datos["userlastname"],
				"usuoppass"=> password_hash($datos["userpass"], PASSWORD_BCRYPT),
				"idrol"    => $datos["userrol"],
				"usuopemail" => $datos["useremail"]
			)
			);
			if(isset($query)){
				return $this->respond(["message" => "success"],200);
			}
			else{
				return $this->respond(["message" => "ocurrio un error"],500);
			}
		}
		else{
			return $this->respond(["message" => "No autorizado"],401);
		}
	}

	//Metodo para obtener un usuario para la edicion
	public function obtenerUsuario(){
		$model = new Usuarios();
		$data = array();
		if($this->request->isAJAX() and $this->session->get('userrol') == 1){
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))),TRUE);
			$query = $model->obtenerUsuarioPorId($datos["userid"]);
			if(isset($query)){
				foreach ($query->getResult() as $row) {
					$data["iduser"]   = $row->idusuopr;
					$data["name"]     = $row->usuopnom;
					$data["lastname"] = $row->usuopape;
					$data["email"]    = $row->usuopemail;
					$data["rol"]      = $row->idrol;
				}
				return $this->respond(["message" => "success", "data" => $data],200);
			}
			else{
				return $this->respond(["message" => "not found"],404);
			}
		}
		else{
			return $this->respond(["message" => "No autorizado"],403);
		}
	}

	//Metodo para guardar los datos del usuario editado
	public function editarUsuario(){
		$model = new Usuarios();
		if($this->request->isAJAX() and $this->session->get('userrol') == 1){
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))),TRUE);
			$query = $model->actualizarUsuario(array(
				"idusuopr"   => $datos["userid"],
				"usuopnom"   => $datos["username"],
				"usuopape"   => $datos["userlastname"],
				"usuopemail" => $datos["useremail"],
				"idrol"      => $datos["userrol"],
			));
			if(isset($query)){
				return $this->respond(["message" => "success"],200);
			}
			else{
				return $this->respond(["message" => "error"],500);
			}
		}
		else{
			return $this->respond(["message" => "No autorizado"], 401);
		}
	}

	//Metodo para actualizar la tabla de usuarios
	public function actualizaTabla(){
		$model = new Usuarios();
		$headings = array('ID', "Nombre", "Apellido","Rol de Usuario","Correo Electronico","Acciones");
		$rows = array();
		if($this->request->isAJAX() and $this->session->get('userrol') == 1){
			$query = $model->getAllUsers();
			if(isset($query)){
				foreach($query->getResult() as $row){
					$rows[] = array($row->idusuopr, $row->usuopnom, $row->usuopape,$row->rolnom,$row->usuopemail, '<a class="editar" href="#" id="'.$row->idusuopr.'"> Detalles</a>');
				}
			}
			else{
				$rows[] = array('<td colspan="6">Sin Registros</td>','','','','',"");
			}
			return $this->respond(["message" => "success", "data" => base64_encode($this->generarTabla($headings,$rows))]);
		}
		else{
			return $this->respond(["message" => "No autorizado"],401);
		}
	}
	
}