<?php 
namespace App\Controllers;

use App\Models\Estatus as Status;
use App\Models\Seguimientos;

use CodeIgniter\API\ResponseTrait;

class Estatus extends BaseController{
    use ResponseTrait;
    public function cambioEstatus(){
        $estatusModel = new Status();
        $segModel = new Seguimientos();
        $segQuery = '';
        if($this->request->isAJAX() and $this->session->get('logged')){
            $datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))),TRUE);
            $data = array(
                "idcaso" => $datos["caseid"],
                "idest" => $datos["casestatus"]
            );
            $query = $estatusModel->cambioEstatusCaso($data);
            if(isset($query)){
                //Insertamos en el seguimiento, el cambio de estatus
                switch (intval($data['idest'])) {
                    case 1:
                        $segQuery = $segModel->insertarSeguimiento(
                            array(
                                "idcaso" => $datos["caseid"],
                                "idestllam" => 2,
                                "segcoment" => "Cambiado a estatus Abierto el dia ".date('d-m-Y'),
                                "segfec" => date('Y-m-d'),
                                "idusuopr" => $this->session->get('iduser')
                            )
                        );
                    break;

                    case 2:
                        $segQuery = $segModel->insertarSeguimiento(
                            array(
                                "idcaso" => $datos["caseid"],
                                "idestllam" => 2,
                                "segcoment" => "Cambiado a estatus en Seguimiento el dia ".date('d-m-Y'),
                                "segfec" => date('Y-m-d'),
                                "idusuopr" => $this->session->get('iduser')
                            )
                        );
                    break;

                    case 3:
                        $segQuery = $segModel->insertarSeguimiento(
                            array(
                                "idcaso" => $datos["caseid"],
                                "idestllam" => 2,
                                "segcoment" => "Cambiado a estatus Cerrado el dia ".date('d-m-Y'),
                                "segfec" => date('Y-m-d'),
                                "idusuopr" => $this->session->get('iduser')
                            )
                        );

                    break;

                    case 4:
                        $segQuery = $segModel->insertarSeguimiento(
                            array(
                                "idcaso" => $datos["caseid"],
                                "idestllam" => 2,
                                "segcoment" => "Cambiado a estatus Reabierto el dia ".date('d-m-Y'),
                                "segfec" => date('Y-m-d'),
                                "idusuopr" => $this->session->get('iduser')
                            )
                        );
                    break;
                }
                
                if(isset($segQuery)){
                    return $this->respond(["message" => "Estatus cambiado exitosamente"],200);
                }
                else{
                    return $this->respond(["message" => "Hubo un error al cambiar el estatus"],500);
                }
                
            }
            else{
                return $this->respond(["message" => "Hubo un error al cambiar el estatus"],500);
            }
        }
    }

}