<?php 

namespace App\Controllers;
//Importamos los modelos
use App\Models\Dpt;
use App\Models\RedSocial;
use App\Models\RequerimientoUsuario;
use App\Models\Estatus;
use App\Models\PropiedadIntelectual;
use App\Models\Direcciones; 
use App\Models\Casos;
use App\Models\NizaClasses;
use App\Models\Oficinas;
use App\Models\Seguimientos;
//Importamos las librerias para convertir el controlador en un Endpoint REST
use CodeIgniter\API\ResponseTrait;

class Caso extends BaseController{

	use ResponseTrait;

	//Metodo para cargar la vista del form de nuevo caso
	public function index(){
		if($this->session->get('logged')){
			//Importamos los modelos
			$dptModel = new Dpt();
			$rrssModel = new RedSocial();
			$reqModel = new RequerimientoUsuario();
			$tipoPIModel = new PropiedadIntelectual();
			$dirCorModel = new Direcciones();
			$officeModel = new Oficinas();
			$nizaModel   = new NizaClasses();
			//Consultamos las redes sociales para cargarlas a los forms
			$query = $rrssModel->getAllSocialNet();
			//Inicializamos una cadena para los option que necesitamos por defecto
			$optRS = '';
			$optPais = '';
			$optEstados = '';
			$optTipoPI = '';
			$optDireccion = '';
			$optReq = '';
			$optOfi = '';
			//Arreglo para la vista con los datos
			$data = array();
			//Generamos los option de las redes sociales
			if(isset($query)){
				foreach($query->getResult() as $row){
					$optRS .= '<option value="'.$row->idrrss.'">'.ucfirst($row->rsnom).'</option>';
				}
			}
			else{
				$data["redes_sociales"] = '<option value="NULL">Sin redes sociales cargadas</option>';
			}
			unset($query);
			//Cargamos los paises
			$query = $dptModel->getAllCountries();
			if(isset($query)){
				foreach ($query->getResult() as $row) {
					$optPais .= '<option value="'.$row->paisid.'">'.ucfirst($row->paisnom).'</option>';
				}
			}
			else{
				$data["pais"] = '<option value="NULL">Sin redes sociales cargadas</option>';	
			}
			unset($query);
			//Obtenemos los estados
			$query = $dptModel->getAllStates();
			if(isset($query)){
				foreach ($query->getResult() as $row) {
					$optEstados .= '<option value="'.$row->estadoid.'">'.ucfirst(strtolower($row->estadonom)).'</option>';
				}
			}
			else{
				$data["estados"] = '<option value="NULL">Sin estados</option>';
			}
			unset($query);
			//Obtenemos las direcciones correspondientes
			$query = $dirCorModel->getDirecciones();
			if(isset($query)){
				foreach($query->getResult() as $row){
					$optDireccion .= '<option value="'.$row->iddircor.'">'.ucfirst($row->dircornom).'</option>';
				}
			}
			else{
				$data["direccioncor"] = '<option value="NULL">Sin Direcciones</option>';
			}
			unset($query);
			//Obtenemos los tipos de propiedad Intelectual
			$query = $tipoPIModel->getTipoPI();
			if(isset($query)){
				foreach ($query->getResult() as $row) {
					$optTipoPI .= '<option value="'.$row->idtippropint.'">'.ucfirst($row->tippropnom).'</option>';
				}
			}
			else{
				$data["tipoPI"] = '<option value="NULL">Sin Direcciones</option>';
			}
			unset($query);
			//Obtenemos los requerimientos del usuario
			$query = $reqModel->getTipoRequerimiento();
			if(isset($query)){
				foreach ($query->getResult() as $row) {
					$optReq .= '<option value="'.$row->idtiprequsu.'">'.ucfirst($row->tiprequsunom).'</option>';
				}
			}
			else{
				$data["reqUser"] = '<option value="NULL">Sin Direcciones</option>';
			}
			//obtenemos las clases de Niza
			unset($query);
			//Seteamos los valores en el arreglo
			$data["redes_sociales"] = $optRS;
			$data["pais"] = $optPais;
			$data["estados"] = $optEstados;
			$data["direccioncor"] = $optDireccion;
			$data["tipoPI"] = $optTipoPI;
			$data["reqUser"] = $optReq;
			$data["oficinas"] = $optOfi;

			return $this->loadTemplate('casos/nuevo',$data);
		}
		else{
			return redirect()->to('/');
		}
	}

	//Metodo para obtener el municipio de acuerdo al estado
	public function obtenerMunicipio(){
		$model = new Dpt();
		$opt = '';
		if($this->request->isAJAX() and $this->session->get('logged')){
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))),TRUE);
			$query = $model->getMunicipiosByState($datos["id_estado"]);
			if(isset($query)){
				foreach($query->getResult() as $row){
					$opt .= '<option value="'.$row->municipioid.'">'.ucfirst(strtolower($row->municipionom)).'</option>';
				}
				unset($model);
				return $this->respond(["message" => "success", "data" => $opt],200);
			}
			else{
				unset($model);
				return $this->respond(["message" => "not found"],404);
			}
		}
		else{
			unset($model);
			return redirect()->to('/');
		}
	}

	//Metodo para obtener las parroquias de acuerdo al municipio
	public function obtenerParroquia(){
		$model = new Dpt();
		$opt = '';
		if($this->request->isAJAX() and $this->session->get('logged')){
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))),TRUE);
			$query = $model->getParroquiaByMunicipio($datos["id_municipio"]);
			if(isset($query)){
				foreach($query->getResult() as $row){
					$opt .= '<option value="'.$row->parroquiaid.'">'.ucfirst(strtolower($row->parroquianom)).'</option>';
				}
				unset($model);
				return $this->respond(["message" => "success", "data" => $opt],200);
			}
			else{
				unset($model);
				return $this->respond(["message" => "not found"], 404);
			}
		}
		else{
			unset($model);
			return redirect()->to('/');
		}
	}

	//Metodo para generar un nuevo caso
	public function nuevoCaso(){
		$casoModel = new Casos();
		$tipoPIModel = new PropiedadIntelectual();
		$oficina = new Oficinas();
		$reqModel = new RequerimientoUsuario();
		$segModel = new Seguimientos();
		$nizaModel = new NizaClasses();
		//Arreglo para añadir el nuevo caso
		$newCase = array();
		//Arreglo de direccion de casos
		$dirCaso = array();
		//Arreglo con el tipo de propiedad intelectual
		$tipoPI = array();
		//Arreglo para el requerimiento del usuario
		$reqUsuario = array();
		if($this->session->get('logged') and $this->request->isAJAX()){
			//Obtenemos los datos del formulario
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))),TRUE);
			//llenamos los datos iniciales del caso
			if(empty($datos["pi-type"])){
				return $this->respond(["message" => "Debe seleccionar al menos un tipo de propiedad intelectual"],500);
			}
			else if(empty($datos["user-requirement"])){
				return $this->respond(["message" => "El usuario debe tener al menos un requerimiento"],500);
			}
			else {
				$newCase["casofec"]     = $datos["date-entry"];
				$newCase["casoced"]     = $datos["person-id"];
				$newCase["casonom"]     = $datos["person-name"];
				$newCase["casoape"]     = $datos["person-lastname"];
				$newCase["casotel"]     = $datos["telephone"];
				$newCase["idest"]       = 1;
				$newCase["idrrss"]      = $datos["social_network"];
				$newCase["idusuopr"]    = $this->session->get('iduser');
				$newCase["estadoid"]    = $datos["state"];
				$newCase["municipioid"] = $datos["county"];
				$newCase["parroquiaid"] = $datos["town"];
				$newCase["casociudad "] = $datos["city"];
				$newCase["ofiid"]       = $datos["office"];
				$newCase["casodesc"]    = $datos["user-requirement"];
				if(empty($datos["record-work"])){
					$newCase["casonumsol"] = 'No Aplica';
				}
				else{
					$newCase["casonumsol"] = $datos["record-work"]; 
				}
				//Realizamos la insercion en la tabla
				$query = $casoModel->insertarNuevoCaso($newCase);
				//Obtenemos el id insertado
				$id = $casoModel->getLastID('casos');
				unset($query);
				//Armamos el arreglo para insertar el tipo de propiedad intelectual del caso
				foreach($datos["pi-type"] as $row){
					$tipoPI[] = array(
						"idtippropint" => $row,
						"idcaso"  => $id
					);
				}
				//Hacemos la insercion
				$query2 = $tipoPIModel->insertarTipoPICaso($tipoPI);
				if(isset($query2)){
					foreach($datos["user-requirement"] as $row){
						$reqUsuario[] = array(
							"idtiprequsu" => $row,
							"idcaso"      => $id
						);
					}
					$query3 = $reqModel->insertarRequerimientoUsuario($reqUsuario);
					//Si es exitoso, le decimos al usuario que el caso fue cargado exitosamente
					if(isset($query3)){
						//Realizamos la insercion del seguimiento de apertura para que salga en el TL
						$query4 = $segModel->insertarSeguimiento(array(
							"idcaso" => $id,
							"idestllam" => 1, //Seteamos el valor en 1, que es el estatus por llamar
							"segcoment" => "Caso creado el dia ".date('d-m-Y h:i:s'),
							"segfec" => $datos["date-entry"],
							"idusuopr" => $this->session->get('iduser')
						));
						if(isset($query4)){
							return $this->respond(["message" => "Caso registrado exitosamente, El número de caso es: ".$id],200);
						}
						else{
							return $this->respond(["message" => "Hubo un error en el registro del requerimiento del usuario"],500);	
						}
						
					}
					else{
						return $this->respond(["message" => "Hubo un error en el registro del requerimiento del usuario"],500);
					}
				}
			}
		}
		else{
			return redirect()->to('/');
		}
	}

	//Vista de carga de un caso
	public function vercaso($id){
		$casoModel = new Casos();
		$segModel = new Seguimientos();
		$estModel = new Estatus();
		//Arreglo para los detalles del caso
		$data = array();
		//TimeLine para los seguimientos
		$tlSeguimientos = '';
		//estatus del caso
		$idEstatusCaso = '';
		if($this->session->get('logged')){
			//Consultamos los detalles del caso
			$query = $casoModel->detalleCaso($id);
			if(isset($query)){
				foreach($query->getResult() as $row){
					$data["idcaso"] = $id;
					$data["nombre"] = ucwords(strtolower($row->casonom).' '.strtolower($row->casoape));
					$data["estado"] = ucfirst(strtolower($row->estadonom));
					$data["municipio"] = ucfirst(strtolower($row->municipionom));
					$data["parroquia"] = ucfirst(strtolower($row->parroquianom));
					$data["ciudad"] = ucfirst(strtolower($row->casociudad));
					$data["red_social"] = ucfirst(strtolower($row->rsnom));
					$data["fecha_caso"] = $this->formatearFecha($row->casofec);
					$data["usuario_operador"] = ucwords(strtolower($row->usuopnom)).' '.ucfirst(strtolower($row->usuopape));
					$idEstatusCaso = $row->idest;
				}
			}
			else{
				return redirect()->to('/404');
			}
			unset($query);
			//Consultamos los seguimientos
			$query = $segModel->obtenerSeguimientoDeCaso($id);
			//Generamos los seguimientos
			$tlSeguimientos = $this->getSeguimientos($query);
			//Añadimos los seguimientos a la vista
			$data["seguimientos"] = $tlSeguimientos;
			//Obtenemos los estatus de las llamadas para el select del seguimiento
			unset($query);
			$query = $estModel->estatusLlamadas();
			$estopt = '';
			if(isset($query)){
				foreach($query->getResult() as $row){
					$estopt .= '<option value="'.$row->idestllam.'">'.ucfirst(strtolower($row->estllamnom)).'</option>';
				}
			}
			else{
				$estopt .= '<option value="NULL">Sin estatus</option>';
			}
			$data["estatus"] = $estopt;
			//Obtenemos los estatus de los casos para mostrarlos en el modal
			unset($query);
			$estopt = '';
			$query = $estModel->estatusCaso();
			if(isset($query)){
				foreach($query->getResult() as $row){
					if($row->idest == $idEstatusCaso){
						$estopt .= '<option selected value="'.$row->idest.'">'.ucfirst(strtolower($row->estnom)).'</option>';
					}
					else{
						$estopt .= '<option value="'.$row->idest.'">'.ucfirst(strtolower($row->estnom)).'</option>';
					}
					
				}
			}
			else{
				$estopt .= '<option value="NULL">Sin estatus</option>';
			}
			$data["estatus_llamadas"] = $estopt;
			return $this->loadTemplate('casos/ver_caso', $data);
		}
		else{
			return redirect()->to('/');
		}
	}

	//Vista de listado de casos activos
	public function listadoCasosActivos(){
		//Importamos el modelo
		$casoModel = new Casos();
		//Arreglo para la vista
		$data = array();
		//Arreglos para la tabla a generar
		$rows = array();
		$headings = array("Nº de caso", "Fecha", "Usuario Operador", "Acciones");
		//Hacemos la consulta para obtener los casos por estatus
		$query = $casoModel->obtenerCasosPorEstatus("1");
		if(isset($query)){
			foreach($query->getResult() as $row){
				$rows[] = array($row->idcaso, $this->formatearFecha($row->casofec),ucwords($row->usuopnom.' '.$row->usuopape), '<a href="/verCaso/'.$row->idcaso.'">Detalles</a>');
			}
		}
		else{
			$rows[] =array('<td colspan="4">No hay casos generados</td>','','','');
		}
		//Generamos la tabla
		$data["tabla"] = $this->generarTabla($headings,$rows);
		return $this->loadTemplate('casos/listado_activos',$data);
		
	}

	//Vista del modulo de historico de casos
	public function historicoCasos(){
		//Importamos el modelo
		$casoModel = new Casos();
		//Arreglo para la vista
		$data = array();
		//Arreglos para la tabla a generar
		$rows = array();
		$headings = array("Nº de caso", "Fecha", "Usuario Operador", "Estatus" ,"Acciones");
		//Hacemos la consulta para obtener los casos por estatus activo
		$query = $casoModel->obtenerCasos();
		if(isset($query)){
			foreach($query->getResult() as $row){
				$rows[] = array($row->idcaso, $this->formatearFecha($row->casofec),ucwords($row->usuopnom.' '.$row->usuopape),ucwords($row->estnom) ,'<a href="/verCaso/'.$row->idcaso.'">Detalles</a>');
			}
		}
		//Generamos la tabla
		$data["tabla"] = $this->generarTabla($headings,$rows);
		return $this->loadTemplate('casos/historico',$data);
	}
}