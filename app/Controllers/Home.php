<?php namespace App\Controllers;

use App\Models\Casos;

class Home extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}

	//--------------------------------------------------------------------

	//Todas las vistas deben ser cargadas aqui
	public function login(){
		echo view('template/header');
		echo view('login/content');
		echo view('login/footer');
	}

	//Vista principal

	public function dashboard(){
		$model = new Casos();
		$headings = array('Nº caso', 'Nombre y apellido del usuario', 'Fecha de Recepcion', "Estatus caso", "Acciones");
		$rows = array();
		if($this->session->get('logged')){
			//Obtenemos los 20 ultimos casos generados por el usuario
			$query = $model->obtenerUltimosCasos($this->session->get('iduser'));
			if(isset($query)){
				foreach($query->getResult() as $row){
					$rows[] = array($row->idcaso, $row->casonom.' '.$row->casoape, $this->formatearFecha($row->casofec), $row->estnom, '<a href="/verCaso/'.$row->idcaso.'">Detalles</a>');
				}
			}
			//Pasamos la tabla como parametro para la vista
			return $this->loadTemplate('dashboard',array("tabla" => $this->generarTabla($headings,$rows)));
		}
		else{
			return redirect()->to('/');
		}
	}
}
