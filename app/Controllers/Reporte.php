<?php namespace App\Controllers;

use App\Models\Casos;
use App\Models\Seguimientos;
use CodeIgniter\API\ResponseTrait;

use function PHPUnit\Framework\isNull;

class Reporte extends BaseController{
    protected $graphOptions = array(
        "backgroundColor" => ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de','#5f2b13', '#258ebe', '#257531', '#512575', '#3c8dbc', '#d2d6de','#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de']
    );
    use ResponseTrait;
    /*
    * Metodo para generar las vistas de los reportes
    * @param tipoReporte: tipo de reporte a cargar
    *
    */

	public function tipoReporte($tipoReporte){
        $reporte = intval($tipoReporte);
        switch ($reporte) {
            case 1:
                return $this->loadTemplate("reportes/diario", NULL);      
                break;
            case 2:
                return $this->loadTemplate("reportes/semanal", NULL);      
                break;
            case 3:
                return $this->loadTemplate("reportes/diario", NULL);
                break;
            case 4:
                return $this->loadTemplate("reportes/anual", NULL);
                break;
            default:
                redirect('/404');
                break;
        }
    }

    //Metodo para generar los reportes y mandarlos a la vista
    public function generarReporte(){
        //Importamos los modelos
        $casosModel = new Casos();
        $seguimientosModel = new Seguimientos();
        //Array para los resultados
        $result = array();
        $labels = array();
        $dataSet = array();
        //Obtenemos los datos
        $request = json_decode($this->request->getPost('data'),TRUE);
        //validamos que tipo de reporte es el que consume el endpoint
        switch($request["type-report"]){
            case '1':
            break;
            case '2':
            break;
            case '3':
            break;
            case '4':
                //Obtenemos el consolidado de casos generados por tipo de Requerimiento de usuario
                $queryCases = $casosModel->consolidadoCasosPorFecha($request['dataRange'].'-01-01', $request["dataRange"].'-12-31', $request["tipopi"]); 
                if(!empty($queryCases->getResultArray())){
                    foreach($queryCases->getResultArray() as $row){
                        $labels['casos'][] = $row['tiprequsunom'];
                        $dataSet['casos'][] = intval($row['count']);
                    }
                }
                else{
                    return $this->respond(['message' => 'Sin datos para generar'], 404);
                }
                //Obtenemos ahora el consolidado de casos generados por Propiedad Intelectual
                $queryPI = $casosModel->contarCasosPorPI($request['dataRange'].'-01-01', $request["dataRange"].'-12-31');
                if(!empty($queryPI->getResultArray())){
                    foreach($queryPI->getResultArray() as $row){
                        $labels['casosPI'][]   = $row['tippropnom'];
                        $dataSet['casosPI'][]  = intval($row['cantPI']);
                    }
                }
                else{
                    return $this->respond(['message' => 'Sin datos para generar'], 404);
                }
                return $this->respond(array(
                    'message' => 'success',
                    'data' => array(
                        'labels' => $labels,
                        'dataSet' => $dataSet
                    )
                ), 200);
            break;
        }
        
    }    

}