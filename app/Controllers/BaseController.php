<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
class BaseController extends Controller
{
    /**
     * Instance of the main Request object.
     *
     * @var CLIRequest|IncomingRequest
     */
    protected $request;

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $helpers = [];

    /**
     * Constructor.
     */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        // Preload any models, libraries, etc, here.

        // E.g.: $this->session = \Config\Services::session();
        $this->session = \Config\Services::session();
		$this->cache   = \Config\Services::cache();
    }

    //Metodo para cargar la plantilla
	public function loadTemplate($modulo, Array $data = NULL){
		echo view('template/header');
		if($modulo != 'login'){
			echo view('template/nav_bar');
			if($data != NULL){
				echo view($modulo.'/content', $data);
			}
			else{
				echo view($modulo.'/content');
			}
			echo view('template/footer');
			echo view($modulo.'/footer');
		}
		else{
			echo view('login/content');
			echo view('template/footer');
			echo view('login/footer');
		}
	}

	/*Funcion que formatea fechas*/
    public function formatearFecha($fecha){
        $date1 = explode('-',$fecha);
        $date2 = $date1[2]."-".$date1[1]."-".$date1[0];
        return $date2;
    }
     /**
	 * Metodo que permite generar tablas debidamente formateadas
	 * usando Bootstrap4
	 * 
	 *
	 * @param heading : Array  => Arreglo con las cabeceras del usuario
	 * @param data    : Array   => Los datos de la tabla puestos en un arreglo de arreglos
	 * 
	 */
    public function generarTabla(Array $heading, Array $data){
    	$this->table = new \CodeIgniter\View\Table();

    	$template = [
	        'table_open'        => '<table class="table table-light table-striped text-center tabla">',

	        'thead_open'        => '<thead>',
	        'thead_close'       => '</thead>',

	        'heading_row_start' => '<tr>',
	        'heading_row_end'   => '</tr>',
	        'heading_cell_start'=> '<th>',
	        'heading_cell_end'      => '</th>',

	        'tfoot_open'             => '<tfoot>',
	        'tfoot_close'            => '</tfoot>',

	        'footing_row_start'      => '<tr>',
	        'footing_row_end'        => '</tr>',
	        'footing_cell_start'     => '<td>',
	        'footing_cell_end'       => '</td>',

	        'tbody_open'            => '<tbody>',
	        'tbody_close'           => '</tbody>',

	        'row_start'             => '<tr>',
	        'row_end'               => '</tr>',
	        'cell_start'            => '<td>',
	        'cell_end'              => '</td>',

	        'row_alt_start'         => '<tr>',
	        'row_alt_end'           => '</tr>',
	        'cell_alt_start'        => '<td>',
	        'cell_alt_end'          => '</td>',

	        'table_close'           => '</table>'
	    ];
	    $this->table->setTemplate($template);
	    $this->table->setHeading($heading);
	    return $this->table->generate($data);
	}
	
	//Metodo para generar los seguimientos
	public function getSeguimientos($query){
		$tlSeguimientos = '';
		if(isset($query)){
		//Recorremos el objeto de consulta pero debemos preguntar por el estatus de llamadas
			$data = array();
			foreach($query->getResult() as  $row){
				$data[] = array(
					$row->idsegcas,
					$row->segfec,
					$row->estllamnom,
					$row->usuopnom." ".$row->usuopape,
					$row->segcoment,
				);
			}
			$tlSeguimientos = $this->generarTabla(["Nº seguimiento", "Fecha Seguimiento", "Estatus de Llamada", "Usuario Operador", "Comentario"], $data);
		}
		else{
			$data = array('<td colspan="4">Sin Registros</td>');
			$tlSeguimientos = $this->generarTabla(["Nº seguimiento", "Fecha Seguimiento", "Estatus de Llamada", "Usuario Operador", "Comentario"], $data);
		}
		return $tlSeguimientos;
	}
}
