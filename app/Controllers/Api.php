<?php 
namespace App\Controllers;

use App\Models\Casos;
use CodeIgniter\API\ResponseTrait;


class Api extends BaseController{

    use ResponseTrait;

    //Metodo para autenticar el token
    public function authToken(){
		$casos = new Casos();
		//validamos si la solicitud es via AJAX
		if($this->request->isAJAX()){
            //Obtenemos el api
			$data = $this->request->getGet('apikey');

		}
	}

    //Metodo para generar el token de uso para la API


    
    
    //Metodo para generar la data en excel

    public function excelReport(){
        $casos = new Casos();
        $data = array();
        //aqui se debe validar que el apikey este autorizado
        if($this->request->isAJAX()){
            $dataFrom = $this->request->getGet('dataFrom');
            $dataLast = $this->request->getGet('dataLast');
            $query = $casos->obtenerCasosPorPeriodo($dataFrom, $dataLast);
            if(isset($query)){
                foreach($query->getResult() as $row){
                    $data[] = array(
                        'casonum' => $row->idcaso,
                        'casofec' => $row->casofec,
                        'casoced' => $row->casoced,
                        'casonom' => $row->casonom,
                        'casoape' => $row->casoape,
                        'casotel' => $row->casotel,
                        'casonumsol' => $row->casonumsol,
                        'paisnom' => $row->paisnom,
                        'estadonom' => $row->estadonom,
                        'municipionom' => $row->municipionom,
                        'parroquianom' => $row->parroquianom,
                        'operador' => "'$row->usuopnom' '$row->usuopape'",
                    );
                }
                return $this->respond(['message' => 'success', 'data' => $data],200);
            }
            else{
                return $this->respond(['message'=> 'Bad Format'], 400);
            }
            
        }
        else{
            return $this->respond(['message' => 'Method not Allowed'], 405);
        }
    }


}

?>