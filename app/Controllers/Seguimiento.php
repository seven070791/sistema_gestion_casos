<?php namespace App\Controllers;

use App\Models\Seguimientos;

use CodeIgniter\API\ResponseTrait;

class Seguimiento extends BaseController{

    use ResponseTrait;

    //Metodo para añadir seguimientos 
    public function addSeguimiento(){
        $segModel = new Seguimientos();
        if($this->request->isAJAX() and $this->session->get('logged')){
            $datos = json_decode(utf8_decode(base64_decode($this->request->getPost('data'))),TRUE);
            if(strlen($datos["segcomment"]) < 1){
                return $this->respond(["message" => "El comentario no debe estar en blanco"],500);
            }
            else{
                $query = $segModel->insertarSeguimiento(array(
                    "idcaso" => $datos["caseid"],
                    "idestllam" => $datos["callid"],
                    "segcoment" =>$datos["segcomment"],
                    "segfec" => date('Y-m-d'),
                    "idusuopr" => $this->session->get('iduser')
                ));
                if(isset($query)){
                    return $this->respond(["message" => "Seguimiento cargado exitosamente"],200);
                }
                else{
                    return $this->respond(["message" => "Hubo un error al cargar el seguimiento"],500);
                }
            }
        }
        else{
            return redirect()->to('/');
        }
    }

    public function obtenerTL(){
        $segModel = new Seguimientos();
        if($this->request->isAJAX() and $this->session->get('logged')){
            $datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))),TRUE);
            $query = $segModel->obtenerSeguimientoDeCaso($datos["data"]);
            if(isset($query)){

                return $this->respond(["message" => "success", "data" => $this->getSeguimientos($query)],200);
            }
            else{
                return $this->respond(["message" => "No se encontraron seguimientos"],404);
            }
        }
        else{
            return redirect()->to('/');
        }
    }
	
}