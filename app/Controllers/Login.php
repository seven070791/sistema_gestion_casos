<?php
namespace App\Controllers;

use App\Models\Casos;
use App\Models\Usuarios;
use CodeIgniter\API\ResponseTrait;

class Login extends BaseController{
	
	use ResponseTrait;

	//Metodo para iniciar la sesion
	public function autenticar(){
		$model = new Usuarios();
		$session = session();
		$userdata = array();
		if($this->request->isAJAX()){
			$datos = json_decode(base64_decode($this->request->getPost('data')),TRUE);
			$query = $model->obtenerUsuario($datos["username"]);
			if(isset($query)){
				foreach ($query->getResult() as $row) {
					if(password_verify($datos["userpass"], $row->usuoppass)){
						$userdata["nombre"] = ucwords($row->usuopnom).' '.ucwords($row->usuopape);
						$userdata["userrol"] = $row->idrol;
						$userdata["iduser"] = $row->idusuopr;
						$userdata["logged"] = TRUE;
						$session->set($userdata);
						return $this->respond(["message" => "Iniciando sesion"],200);
					}
					else{
						return $this->respond(["message" => "Clave incorrecta"],401);
					}
				}
			}
			else{
				return $this->respond(["message" => "Usuario o contraseña incorrectos"],404);
			}
		}
		else{
			return redirect()->to('/');
		}
	}

	//Metodo para cerrar la sesion
	public function logout(){
		$this->session->destroy();
		return redirect()->to('/');
	}

}