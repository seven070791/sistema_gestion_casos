<?php namespace App\Models;

class PropiedadIntelectual extends BaseModel{

	//Metodo para obtener todos los tipos de propiedad Intelectual
	public function getTipoPI(){
		$builder = $this->dbconn('sgc_tipo_prop_intelec');
		$query = $builder->get();
		return $query;
	}

	//Metodo para insertar los tipos de propiedad intelectual de los casos
	public function insertarTipoPICaso(Array $datos){
		$builder = $this->dbconn('sgc_tipo_prop_caso');
		$query = $builder->insertBatch($datos);
		return $query;
	}
}