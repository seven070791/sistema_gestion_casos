<?php 
namespace App\Models;

class Seguimientos extends BaseModel{
    //Metodo para obtener los seguimientos de un caso
    public function obtenerSeguimientoDeCaso(String $idcaso){
        $builder = $this->dbconn('sgc_seguimiento_caso a');
        $builder->join("sgc_estatus_llamadas b", "a.idestllam = b.idestllam");
        $builder->join("sgc_usuario_operador c", "a.idusuopr = c.idusuopr");
        $builder->join('sgc_casos d', "a.idcaso = d.idcaso");
        $builder->join('sgc_estatus e', "d.idest = e.idest");
        $builder->where("a.idcaso", $idcaso);
        $builder->orderBy('a.segfec','ASC');
        $query = $builder->get();
        return $query;
    }

    //Metodo para insertar un seguimiento del caso
    public function insertarSeguimiento(Array $datosSeguimiento){
        $builder = $this->dbconn("sgc_seguimiento_caso");
        $query = $builder->insert($datosSeguimiento);
        return $query;
    }

    //Metodo para la consulta de los seguimientos por fecha 
    public function consultaSeguimientoPorFecha(Array $datos){
        $builder = $this->dbconn("sgc_seguimiento_caso a");
        $builder->select("a.idsegcas, b.estllamnom, a.segcoment, a.segfec, c.usuopnom, c.usuopape");
        $builder->join("sgc_estatus_llamadas b", "a.idestllam = b.idestllam");
        $builder->join("sgc_usuario_operador c", "a.idusuopr = c.idusuopr");
        $builder->where("a.segfec BETWEEN '".$datos["fecha_inicio"]."' AND '".$datos["fecha_fin"]."'");
        $query = $builder->get();
        return $query;
    }

    //Metodo para obtener contadores de la tabla de seguimientos
    public function  contadoresSeguimientos(Array $datos){
        $builder = $this->dbconn("sgc_seguimiento_caso a");
        $builder->select("b.estllamnom, COUNT(a.idestllam)");
        $builder->join("sgc_estatus_llamadas b", "a.idestllam = b.idestllam");
        $builder->where("a.segfec BETWEEN '".$datos["fecha_inicio"]."' AND '".$datos["fecha_fin"]."'");
        $builder->groupBy('b.estllamnom');
        $query = $builder->get();
        return $query;
    }
}