<?php 

namespace App\Models;

class Casos extends BaseModel{
    //Metodo para insertar un nuevo caso en la BD
    public function insertarNuevoCaso(Array $datos){
        $builder = $this->dbconn('sgc_casos');
        $query = $builder->insert($datos);
        return $query;
    }

    //Metodo para consultar los ultimos veinte casos por usuario

    public function obtenerUltimosCasos(String $iduser){
        $builder = $this->dbconn('sgc_casos a');
        $builder->select('a.idcaso,a.casonom, a.casoape, a.casofec, b.estnom');
        $builder->join('sgc_estatus b', "b.idest = a.idest");
        $builder->where('a.idusuopr', $iduser);
        $builder->orderBy('a.idcaso', 'DESC');
        $builder->limit(20);
        $query = $builder->get();
        return $query;
    }

    //Metodo para obtener el detalle de un solo caso
    public function detalleCaso(String $idcaso){
        $builder = $this->dbconn('sgc_casos a');
        $builder->join('sgc_estatus b', "b.idest = a.idest");
        $builder->join('sgc_usuario_operador c' , "a.idusuopr = c.idusuopr");
        $builder->join("sgc_red_social d","d.idrrss = a.idrrss");
        $builder->join("sgc_estados e", "e.estadoid = a.estadoid");
        $builder->join("sgc_municipio f", "f.municipioid = a.municipioid");
        $builder->join("sgc_parroquias g", "g.parroquiaid = a.parroquiaid");
        $builder->where("a.idcaso", $idcaso);
        $query = $builder->get();
        return $query;
    }

    //Metodo para obtener los casos por estatus
    public function obtenerCasosPorEstatus(String $estatus){
        $builder = $this->dbconn('sgc_casos a');
        $builder->join("sgc_estatus b", "b.idest = a.idest");
        $builder->join('sgc_usuario_operador c' , "a.idusuopr = c.idusuopr");
        $builder->where("a.idest", $estatus);
        $query = $builder->get();
        return $query;
    }

    //Metodo para obtener todos los casos
    public function obtenerCasos(){
        $builder = $this->dbconn('sgc_casos a');
        $builder->join("sgc_estatus b", "b.idest = a.idest");
        $builder->join('sgc_usuario_operador c' , "a.idusuopr = c.idusuopr");
        $builder->orderBy('a.casofec', 'ASC');
        $query = $builder->get();
        return $query;
    }

    //Metodo para obtener los casos para los reportes
    public function obtenerCasosConsolidados(String $endDate, String $initDate){
        $builder = $this->dbconn('sgc_casos a');
        $builder->select("a.idcaso, a.casofec, a.casoced, a.casonom, a.casoape, a.casotel, a.casonumsol, a.casoavz, b.estnom, c.rsnom, d.usuopnom, d.usuopape, e.estadonom, f.paisnom, g.municipionom, h.parroquianom");
        $builder->join("sgc_estatus b", "a.idest = b.idest");
        $builder->join('sgc_red_social c', "a.idrrss = c.idrrss");
        $builder->join('sgc_usuario_operador d', "a.idusuopr = d.idusuopr");
        $builder->join("sgc_estados e", "a.estadoid = e.estadoid");
        $builder->join("sgc_paises f", "e.paisid = f.paisid");
        $builder->join("sgc_municipio g", "a.municipioid = g.municipioid");
        $builder->join("sgc_parroquias h", "a.parroquiaid = h.parroquiaid");
        $builder->where("a.casofec BETWEEN '".$initDate."' AND '".$endDate."'");
        $builder->orderBy('a.idcaso', "ASC");
        $query = $builder->get();
        return $query;
    }

    //Metodo para obtener los casos por fecha
    public function contarCasosPorFecha(String $startDate, String $endDate){
        $builder = $this->dbconn('sgc_casos');
        $builder->select('casofec');
        $builder->selectCount('casofec', 'cantCases');
        $builder->where("casofec BETWEEN '$startDate' AND '$endDate'");
        $builder->groupBy('casofec');
        $result = $builder->get();
        return $result;
    }

    //Metodo que cuenta los casos por propiedad Intelectual
    public function contarCasosPorPI(String $startDate, String $endDate){
        $builder = $this->dbconn('sgc_casos a');
        $builder->select('c.tippropnom');
        $builder->join('sgc_tipo_prop_caso b', 'a.idcaso = b.idcaso');
        $builder->join('sgc_tipo_prop_intelec c', "b.idtippropint = c.idtippropint");
        $builder->where("a.casofec BETWEEN '$startDate' AND '$endDate'");
        $builder->groupBy("c.tippropnom");
        $builder->selectCount('c.tippropnom', 'cantPI');
        $query = $builder->get();
        return $query;
    }

    //Metodo que retorna todos los casos generados en el periodo solicitado
    public function obtenerCasosPorPeriodo(String $fromDate, String $lastDate){
        $builder = $this->dbconn('sgc_casos a');
        $builder->select('a.idcaso, a.casofec, a.casoced, a.casonom, a.casoape, a.casotel, a.casonumsol, c.paisnom, b.estadonom, d.municipionom, e.parroquianom, f.usuopnom, f.usuopape');
        $builder->join('sgc_estados b', 'a.idest = b.estadoid');
        $builder->join('sgc_paises c', "b.paisid = c.paisid");
        $builder->join('sgc_municipio d', "d.municipioid = a.municipioid");
        $builder->join('sgc_parroquias e', "e.parroquiaid = a.parroquiaid");
        $builder->join('sgc_usuario_operador f', "f.idusuopr = a.idusuopr");
        $builder->where("a.casofec BETWEEN '$fromDate' AND '$lastDate'");
        $builder->orderBy('a.idcaso', 'ASC');
        $result = $builder->get();
        return $result;
    }

    //Metodo que retorna los consolidados de los casos generados
    public function consolidadoCasosPorFecha(String $fromDate, String $lastDate, String $tipoPI){
        $builder = $this->dbconn('sgc_casos a');
        $builder->select('c.tiprequsunom, COUNT(b.idtiprequsu)');
        $builder->join('sgc_tipo_req_usu_caso b', 'a.idcaso = b.idcaso');
        $builder->join('sgc_tipo_req_usu c', 'b.idtiprequsu = c.idtiprequsu');
        $builder->where("a.casofec BETWEEN '".$fromDate."' AND '".$lastDate."'");
        $builder->where("c.tipreqpi = '".$tipoPI."'");
        $builder->groupBy('c.tiprequsunom');
        $result = $builder->get();
        return $result;
    }


}