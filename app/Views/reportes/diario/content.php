<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12 p-2">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Reporte diario de casos y seguimientos</h3>
                    <button class="btn btn-light" id="generaArchivoExcel"><i class="fas fa-file-excel"></i> Generar Archivo Excel</button>
                </div>
              </div>
              <div class="card-body">
                <!--Form-->
                <form id="consulta-reporte" name="consulta-reporte" method="POST" action="/reporte">
                  <input name="typeReport" id="typeReport" type="hidden" value="1">  
                    <div class="row">
                      <div class="col-5">
                        <label for="dataRange">Rango de consulta</label>
                        <input type="text" class="form-control" id="dataRange" name="dataRange" placeholder="Haga clic para seleccionar rango de consulta" required>
                      </div>
                      <div class="col-5">
                        <label for="typeConsult">Tipo de reporte</label>
                        <select id="typeConsult" name="typeConsult" class="form-control">
                          <option value="1">Casos</option>
                          <option value="2">Seguimientos</option>
                        </select>
                      </div>
                      <div class="col-2">
                        <div class="p-3"></div>
                        <button type="submit" class="btn btn-primary boton">Consultar</button>
                      </div>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12 p-2">
            <div class="card">
              <div class="card-header">
              </div>
              <div class="card-body">
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-info">
                    <div class="inner">
                      <h3>150</h3>
                      <p>Cantidad de usuarios atendidos</p>
                    </div>
                    <div class="icon">
                      <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <div class="col-lg-9 col-9">
                  <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
                    <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>                         
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
