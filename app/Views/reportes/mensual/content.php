<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12 p-2">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Reporte diario de casos y seguimientos</h3>
                    <button class="btn btn-light" id="generaArchivoExcel"><i class="fas fa-file-excel"></i> Generar Archivo Excel</button>
                </div>
              </div>
              <div class="card-body">
                <!--Form-->
                <form id="consulta-reporte" name="consulta-reporte" method="POST">
                <input name="typeReport" id="typeReport" type="hidden" value="4">  
                <div class="row">
                    <div class="col-2">
                      <label for="rango-consulta">Año</label>
                      <select class="form-control name="rango-consulta" id="rango-consulta">
                        <?php for($i = 2020; $i <= intval(date('Y')); $i++){ ?>
                          <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php } ?>
                        </select>
                    </div>
                    <div class="col-2">
                      <label for="rango-consulta">Mes</label>
                      <select class="form-control name="rango-consulta" id="rango-consulta">
                        </select>
                    </div>
                    <div class="col-5">
                      <label for="tipo-reporte">Tipo de reporte</label>
                      <select id="tipo-reporte" name="tipo-reporte" class="form-control">
                        <option value="1">Casos</option>
                        <option value="2">Seguimientos</option>
                      </select>
                    </div>
                    <div class="col-2">
                      <div class="p-3"></div>
                      <button type="submit" class="btn btn-primary boton">Consultar</button>
                    </div>
                  </div>
                </form>
                <!--Chart-->
                <div class="row" id="grafico">
                  <div class="col-4 p-4">
                    <div class="d-flex">
                      <p class="d-flex flex-column">
                        <span class="text-bold text-lg">Grafico de operaciones</span>
                      </p>
                    </div>
                    <!-- /.d-flex -->
                    <div class="position-relative mb-4">
                      <canvas id="salida" height="200"></canvas>
                    </div>
                  </div>
                </div>
                <!-- /Chart-->
                <!--Table result-->
                <div class="row">
                  <div class="col-12 p-2">
                    <div id="tabla">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
