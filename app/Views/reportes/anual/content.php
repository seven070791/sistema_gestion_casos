<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Reporte Anual</h1>
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Generacion de reportes</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <form id="anual-report" name="anual-report" method="POST" class="form-horizontal">
          <input type="hidden" id="type-report" name="name-report" value="4">
          <div class="card-body">
            <div class="form-group row">
              <label for="datarange" class="col-sm-2 col-form-label">Año de consulta</label>
              <div class="col-sm-4">
                <select id="dataRange" name="dataRange" class="form-control">
                  <?php for($i = 2020; $i <= intval(date('Y')); $i++){ ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php  } ?>
                </select>
              </div>
              <label for="tipopi" class="col-sm-2 col-form-label">Tipo consulta</label>
              <div class="col-sm-3">
                <select id="tipopi" name="tipopi" class="form-control">
                  <option value="1">Marcas</option>
                  <option value="2">Patentes</option>
                  <option value="3">Derecho de Autor</option>
                </select>
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary">Consultar</button>
              </div>
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            
          </div>
          <!-- /.card-footer-->
        </form>
      </div>
      <!-- /.card -->

      <div class="row">
        <div class="col-6">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Casos Generados</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body">
            <canvas id="casesDonut" style="max-height: 100%; max-width: 100%;"></canvas>
            </div>
          </div>
        </div>

        <div class="col-6">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Casos por Propiedad Intelectual</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
            </div>
            <div class="card-body">
            <canvas id="casesPIDonut" style="max-height: 100%; max-width: 100%;"></canvas>
            </div>
          </div>
        </div>
      </div>

      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
