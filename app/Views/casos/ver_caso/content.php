<?php 
$session = session();
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            
          </div>
          <div class="col-sm-6">
            
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Detalles del caso</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1" id="tl">
              <?php echo $seguimientos;?>
            </div>
            <!--Detalles del caso-->
            <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
              <h3 class="text-primary"><i class="fas fa-angle-double-right"></i> Caso Nº <?php echo $idcaso;?> </h3>
              <input type="hidden" id="id-caso" name="id-caso" value="<?php echo $idcaso?>">
              <br>
              <div class="text-muted">
                <p class="text-sm">Nombre y Apellido
                  <b class="d-block"><?php echo $nombre;?></b>
                </p>
                <p class="text-sm">
                  Estado: <b class="d-block"><?php echo $estado;?></b>
                  Municipio: <b class="d-block"><?php echo $municipio;?></b>
                  Parroquia <b class="d-block"><?php echo $parroquia;?></b>
                  Ciudad <b class="d-block"><?php echo $ciudad;?></b>
                </p>
              </div>

              <h5 class="mt-5 text-muted">Detalles adicionales</h5>
              <ul class="list-unstyled">
                <li>
                  <i class="far fa-user fa-user"></i> <?php echo $usuario_operador;?>
                </li>
                <li>
                  <i class="far fa-calendar fa-calendar"></i> <?php echo $fecha_caso;?>
                </li>
                <li>
                  <i class="fas fa-rss fa-rss"></i> <?php echo $red_social;?>
                </li>
              </ul>
              <div class="text-center mt-5 mb-3">
                <a data-toggle="modal" data-target="#add-seguimiento" class="btn btn-sm btn-primary">Añadir Seguimiento</a>
                <?php if ($session->get('userrol') == 1 || $session->get('userrol') == 2){?>
                <a data-toggle="modal" data-target="#cambiar-estatus" class="btn btn-sm btn-warning">Cambiar estatus</a>
              <?php }?>
              </div>
            </div>
            <!--/Detalles del caso-->
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!--Añadir seguimiento-->
  <div class="modal fade" id="add-seguimiento">
    <div class="modal-dialog modal-lg">
      <form id="new-seguimiento" method="POST">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Añadir Segumiento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="estatus-llamadas">Estatus de llamadas</label>
              <select class="form-control" id="estatus-llamadas" name="estatus-llamadas">
                <?php echo $estatus;?>
              </select>
            </div>
            <div class="form-group">
              <label for="seguimiento-comentario">Añadir seguimiento</label>
              <textarea id="seguimiento-comentario" name="seguimiento-comentario" class="form-control"></textarea>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="reset" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Añadir seguimiento</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </form>
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <!--/.Añadir seguimiento-->
  <?php if($session->get('userrol') == 1 or $session->get('userrol') == 2){?>
  <div class="modal fade" id="cambiar-estatus">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form id="caso-estatus" method="POST">
          <div class="modal-header">
            <h4 class="modal-title">Cambiar estatus de caso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="estatus-caso">Estatus caso</label>
              <select id="estatus-caso" name="estatus-caso" class="form-control">
                <?php echo $estatus_llamadas;?>
              </select>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="reset" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar cambios</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<?php }?>
