<div class="my-3 my-md-5">
  <div class="container">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12 col-sm-12">
        <!-- general form elements -->
        <div class="card card-light">
          <div class="card-header">
            <h3 class="card-title">Nuevo Caso</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" id="new-case" name="new-case">
            <div class="card-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-6">
                    <label for="red-social">Red Social</label>
                    <select class="form-control" name="red-social" id="red-social">
                      <?php echo $redes_sociales;?>
                    </select>
                  </div>
                  <div class="col-3">
                    <label for="">Oficina</label>
                    <select class="form-control" name="office" id="office">
                      <option value="1">Sala situacional</option>
                      <option value="2">Coordinacion Regional</option>
                    </select>
                  </div>
                  <div class="col-3">
                    <label for="fecha-recibido">Fecha de Recibido</label>
                    <input class="form-control" type="text" name="fecha-recibido" id="fecha-recibido" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-3">
                    <label for="nombre-persona">Nombre</label>
                    <input type="text" class="form-control" name="nombre-persona" id="nombre-persona" required>
                  </div>
                  <div class="col-3">
                    <label for="apellido-persona">Apellido</label>
                    <input type="text" class="form-control" name="apellido-persona" id="apellido-persona" required>
                  </div>
                  <div class="col-3">
                    <label for="tipo-persona">Tipo Persona</label>
                    <select class="form-control" id="tipo-persona" name="tipo-persona">
                      <option value="V">V - Venezolano</option>
                      <option value="E">E - Extranjero</option>
                      <option value="J">J - Juridico</option>
                      <option value="G">G - Gobierno</option>
                    </select>
                  </div>
                  <div class="col-3">
                    <label for="cedula-persona">Nº cedula o Rif</label>
                    <input type="text" class="form-control" name="cedula-persona" id="cedula-persona" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-2">
                    <label for="telefono-persona">Telefono</label>
                    <input type="text" class="form-control" name="telefono" id="telefono">
                  </div>
                  <div class="col-2">
                    <label for="pais-caso">Pais</label>
                    <select id="pais-caso" name="pais-caso" class="form-control">
                      <?php echo $pais;?>
                    </select>
                  </div>
                  <div class="col-2">
                    <label for="estado-caso">Estado</label>
                    <select id="estado-caso" name="estado-caso" class="form-control">
                      <?php echo $estados;?>
                    </select>
                  </div>
                  <div class="col-2">
                    <label for="municipio-caso">Municipio</label>
                    <select id="municipio-caso" name="municipio-caso" class="form-control">
                      <option>Seleccione estado</option>
                    </select>
                  </div>
                  <div class="col-2">
                    <label for="parroquia-caso">Parroquia</label>
                    <select id="parroquia-caso" name="parroquia-caso" class="form-control">
                      <option>Seleccione parroquia</option>
                    </select>
                  </div>
                  <div class="col-2">
                    <label for="ciudad-caso">Ciudad</label>
                    <input type="text" class="form-control" name="ciudad-caso" id="ciudad-caso" required>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-3">
                   <label for="num-tramite">Nº Tramite/Solicitud</label>
                   <input type="text" class="form-control" name="num-tramite" id="num-tramite" placeholder="Dejar en blanco si no aplica">
                  </div>
                  <div class="col-9">
                    <label for="tipo-pi">Tipo Propiedad Intelectual</label>
                    <select class="select2" multiple="multiple" name="tipo-pi" id="tipo-pi" style="width: 100%;">
                      <?php echo $tipoPI;?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <label for="requerimiento-usuario">Requerimiento del Usuario</label>
                  <select class="select2" multiple="multiple" name="requerimiento-usuario" id="requerimiento-usuario" style="width: 100%;">
                    <?php echo $reqUser;?>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <label for="planteamiento-caso">Planteamiento del caso</label>
                  <textarea type="text" class="form-control" name="planteamiento-caso" id="planteamiento-caso" required>
                  </textarea>
                </div>
              </div>
              <br />
              
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
          </form>
        </div>
        <!-- /.card -->
      </div>
    </div>
  </div>
</div>