<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Creado por Bryan Useche
    </div>
    <!-- Default to the left -->
    <strong>Copyleft &copy; 2020 <a href="http://sapi.gob.ve">Servicio Autónomo de la Propiedad Intelectual</a>.</strong>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo base_url();?>/theme/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>/theme/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url();?>/theme/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>/theme/dist/js/adminlte.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url();?>/theme/plugins/moment/moment.min.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>/theme/plugins/daterangepicker/daterangepicker.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>/theme/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>/theme/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!--Core Custom scripts-->
<script src="<?php echo base_url();?>/custom/js/core.js"></script>