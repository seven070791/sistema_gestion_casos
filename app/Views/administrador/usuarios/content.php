<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title m-0">Usuarios Operadores</h5>
                <div class="card-tools">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUser">
                    <i class="fa fa-plus"></i> Añadir Usuario
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div id="tabla">
                  <?php echo $tabla;?>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal para añadir usuarios-->
  <div class="modal fade" id="addUser">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Registro nuevo Usuario</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="new-user" method="POST" role="form">
          <div class="modal-body">
            <div class="form-group">
              <label for="user-name">Nombre</label>
              <input type="text" name="user-name" id="user-name" class="form-control" placeholder="Ej: Juan">
            </div>
            <div class="form-group">
              <label for="user-lastname">Apellido</label>
              <input type="text" name="user-lastname" id="user-lastname" class="form-control" placeholder="Ej: Perez">
            </div>
            <div class="form-group">
              <label for="user-email">Correo electronico</label>
              <input type="email" name="user-email" id="user-email" class="form-control" placeholder="Ej: juan.perez@sapi.gob.ve">
            </div>
            <div class="form-group">
              <label for="user-rol">Rol de usuario</label>
              <select class="form-control" id="user-rol" name="user-rol">
                <?php echo $roles;?>
              </select>
            </div>
            <div class="form-group">
              <label for="user-pass">Contraseña</label>
              <input type="password" name="user-pass" id="user-pass" class="form-control">
            </div>
            <div class="form-group">
              <label for="user-pass">Confirmar Contraseña</label>
              <input type="password" name="user-confirm-pass" id="user-confirm-pass" class="form-control">
            </div>
          </div>
          <div class="modal-footer ">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            <button class="btn btn-primary" type="submit">Guardar</button>
            <button class="btn btn-light" type="reset">Limpiar</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <!-- Modal para editar usuarios-->
  <div class="modal fade" id="editUser">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Editar Usuario</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="edit-user" method="POST" role="form">
          <input type="hidden" name="userid" id="userid">
          <div class="modal-body">
            <div class="form-group">
              <label for="user-name">Nombre</label>
              <input type="text" name="edit-user-name" id="edit-user-name" class="form-control" placeholder="Ej: Juan">
            </div>
            <div class="form-group">
              <label for="user-lastname">Apellido</label>
              <input type="text" name="edit-user-lastname" id="edit-user-lastname" class="form-control" placeholder="Ej: Perez">
            </div>
            <div class="form-group">
              <label for="user-email">Correo electronico</label>
              <input type="email" name="edit-user-email" id="edit-user-email" class="form-control" placeholder="Ej: juan.perez@sapi.gob.ve">
            </div>
            <div class="form-group">
              <label for="user-rol">Rol de usuario</label>
              <select class="form-control" id="edit-user-rol" name="edit-user-rol">
                <?php echo $roles;?>
              </select>
            </div>
          </div>
          <div class="modal-footer ">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            <button class="btn btn-primary" type="submit">Guardar</button>
            <button class="btn btn-light" type="reset">Limpiar</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
