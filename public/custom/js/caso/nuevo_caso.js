function calendario(selector){
    $(selector).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxDate: fecha(),
        locale: {
            format: 'DD/MM/YYYY',
            daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ]
        }
    });
}

/*Funcion que realiza la fecha de hoy*/
function fecha(){
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1;
    var yy = hoy.getFullYear();
    var fecha = '';
    if(dd<10){
        dd = '0'+dd;
    }
    else if(mm<10){
        mm = '0'+mm;
    }
    fecha = dd+"/"+mm+"/"+yy;
    return fecha;
}

function invertirFecha(fecha){
	let fectmp = fecha.split('/');
	let fechadb = `${fectmp[2]}-${fectmp[1]}-${fectmp[0]}`;
	return fechadb;
}

$("#fecha-recibido").on('focus', function(){
	calendario(this);
})

//Inicializacion de los selects
//Initialize Select2 Elements
$('.select2').select2({
	theme:"bootstrap4"
})
//Initialize Select2 Elements
$('.select2bs4').select2({
	theme: 'bootstrap4'
})


//Evento que busca los municipios por estados
$(document).on('click', "#estado-caso", (e) => {
	e.preventDefault();
	let datos = {
		"id_estado" : $("#estado-caso").val()
	}
	$.ajax({
		url:"/municipios",
		method:"POST",
		dataType:"JSON",
		data : {
			"data":btoa(JSON.stringify(datos))
		}
	}).then((response) => {
		$("#municipio-caso").html(response.data);
	}).catch((request) => {
		Swal.fire('Error', response.JSONmessage, 'Error');
	});
})

//Evento que busca las parroquias por municipio
$(document).on('click', "#municipio-caso", (e) => {
	e.preventDefault();
	let datos = {
		"id_municipio" : $("#municipio-caso").val()
	}
	$.ajax({
		url:"/parroquias",
		method:"POST",
		dataType:"JSON",
		data : {
			"data":btoa(JSON.stringify(datos))
		}
	}).then((response) => {
		$("#parroquia-caso").html(response.data);
	}).catch((request) => {
		Swal.fire('Error', response.JSONmessage, 'Error');
	});
});

//Evento de envio del formulario
$(document).on('submit', "#new-case", function(e){
	e.preventDefault();
	let datos = {
		"social_network"  : $("#red-social").val(),
		"date-entry"      : invertirFecha($("#fecha-recibido").val()),
		"person-name"     : $("#nombre-persona").val(),
		"person-lastname" : $("#apellido-persona").val(),
		"person-id"       : $("#tipo-persona").val()+$("#cedula-persona").val(),
		"telephone"       : $("#telefono").val(),
		"country"         : $("#pais-caso").val(),
		"state"           : $("#estado-caso").val(),
		"county"          : $("#municipio-caso").val(),
		"town"            : $("#parroquia-caso").val(),
		"city"            : $("#ciudad-caso").val(),
		"record-work"     : $("#num-tramite").val(),
		"pi-type"         : $("#tipo-pi").val(),
		"user-requirement": $("#requerimiento-usuario").val(),
		"office"		  : $("#office").val(),
		"nClasification"  : $("#clasificadorNiza").val()
	}
	$.ajax({
		url:"/registrarCaso",
		method:"POST",
		dataType:"JSON",
		data: {
			"data":btoa(JSON.stringify(datos))
		},
		beforeSend:function(){
			$("button[type=submit]").attr('disabled','true');
		}
	}).then((response) => {
		Swal.fire({
			icon: 'success',
			title: "Exito",
			text:response.message,
			showCancelButton: false,
			showConfirmButton: true,
		  }).then((answer) => {
			  if(answer.value){
				  window.location = '/inicio';
			  }
		  })
	}).catch((request) => {
		Swal.fire("Error", request.responseJSON.message,"error");
	});
	$("button[type=submit]").removeAttr('disabled');

});