$(document).on('submit', '#caso-estatus', function(e){
    e.preventDefault();
    let datos = {
        "casestatus": $("#estatus-caso").val(),
        "caseid": $("#id-caso").val(),
    }
    $.ajax({
        url:"/cambiarEstatus",
        method:"POST",
        dataType:"JSON",
        data:{
            "data":btoa(JSON.stringify(datos))
        },
        beforeSend:function(){
            $("button[type=submit]").attr('disabled', "true");
        }
    }).then((response) => {
        Swal.fire("Exito", response.message, "success");
        $("#cambiar-estatus").modal('hide');
        $("#caso-estatus")[0].reset();
    }).catch((request) => {
        Swal.fire("Error", request.responseJSON.message,"error");
    });
    actualizarTL();
    $("button[type=submit]").removeAttr('disabled')
})