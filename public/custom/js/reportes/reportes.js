
//Manejador para el calendario
$("#dataRange").on('focus', function(e){
    if($("#dataRange").val() != 4){
        calendario(this);
    } 
});

//Envio de consulta al backend para devolver la respuesta

$("#consulta-reporte").on('submit', function(e) {
    e.preventDefault();
    rangoConsulta = '';
    if($("#typeReport").val() == 4){
        rangoConsulta = $("#rango-consulta").val();
    }
    else{
        rangoConsulta = invertirFecha($("#rango-consulta").val());
    }
    data = {
        "dataRange"  : rangoConsulta,
        "typeConsult": $("#tipo-reporte").val(),
        "typeReport" : $("#typeReport").val(),
        "typePI"     : $("#tipo-pi").val()   
    }
    $.ajax({
        url: "/reporte",
        method: "POST",
        data : {
            data:JSON.stringify(data)
        },
    }).then(function(response){
        var casos = document.getElementById('salida').getContext('2d');
        var seguimientos = document.getElementById('salida-estatus').getContext('2d');
        new Chart(seguimientos, {
            type: 'bar',
            data : {
                labels   : response.data.labels.seguimientos,
                datasets : [
                    {
                        label: 'Seguimientos',
                        data : response.data.dataset.seguimientos,
                        borderWidth: 1
                    }
                ]
            }
        })
		var myChart = new Chart(casos, {
			type: 'pie',
			data: {
				labels: response.data.labels,
				datasets: [{
					label: 'Casos Generados',
					data: response.data.dataset,
                    backgroundColor: response.data.graphOptions.backgroundColor
				}],
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero: true
						}
					}]
				}
			}
        });
        

    }).catch(function(request){

    });
});




$("#anual-report").on('submit', function(e) {
    e.preventDefault();

    data = {
        'dataRange'  : $("#dataRange").val(),
        'tipopi'     : $("#tipopi").val(),
        'type-report': $("#type-report").val()
    }

    $.ajax({
        url: "/reporte",
        method: "POST",
        data : {
            data:JSON.stringify(data)
        },
    }).then(function(response){
        //Obtenidos los datos del backend, es hora de representarlos
        //Representamos los datos de los casos
        const $grafica = document.querySelector("#casesDonut");
        // Las etiquetas son las que van en el eje X. 
        const etiquetas = response.data.labels.casos;
        // Podemos tener varios conjuntos de datos. Comencemos con uno
        const datosCasos = {
            label: "Casos Generados en el año "+$("#dataRange").val(),
            data: response.data.dataSet.casos, // La data es un arreglo que debe tener la misma cantidad de valores que la cantidad de etiquetas
            backgroundColor: 'rgba(54, 162, 235, 0.2)', // Color de fondo
            borderColor: 'rgba(54, 162, 235, 1)', // Color del borde
            borderWidth: 1,// Ancho del borde
        };
        //representamos los graficos
        new Chart($grafica, {
            type: 'pie',// Tipo de gráfica
            data: {
                labels: etiquetas,
                datasets: [
                    datosCasos,
                    // Aquí más datos...
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                },
            }
        });

        new Chart(document.querySelector("#casesPIDonut"), {
            type: 'pie',// Tipo de gráfica
            data: {
                labels: response.data.labels.casosPI,
                datasets: [
                    response.data.dataSet.casosPI,
                    // Aquí más datos...
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                },
            }
        });
    }).catch(function(request){
        Swal.fire("Error", request.responseJSON.message,"error");
    });

});


