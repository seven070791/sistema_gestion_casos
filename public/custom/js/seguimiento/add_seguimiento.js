function actualizarTL(){
    $.ajax({
        url:"/gettl",
        method:"POST",
        dataType:"JSON",
        data: {
            "data":btoa(JSON.stringify({"data": $("#id-caso").val()}))
        }
    }).then((response) => {
        $("#tl").html(response.data);
    });
}

$(document).on('submit', "#new-seguimiento", function(e){
    e.preventDefault();
    let datos = {
        "callid" : $("#estatus-llamadas").val(),
        "segcomment": $("#seguimiento-comentario").val(),
        "caseid" : $("#id-caso").val()
    }
    if(datos.segcomment.lenght < 1){
        Swal.fire("Atencion", "Debe añadir obligatoriamente un comentario", "error");
    }
    else{
        $.ajax({
            url:"/addSeguimiento",
            method:"POST",
            dataType:"JSON",
            data: {
                "data" : btoa(JSON.stringify(datos))
            },
            beforeSend: function(){
                $("button[type=submit]").attr('disabled','true');
            }
        }).then((response) => {
            Swal.fire('Exito', response.message, "success");
            $("#new-seguimiento")[0].reset();
            actualizarTL();
        }).catch((request) => {
            Swal.fire("Error", request.responseJSON.message,"error");
        });
        $("button[type=submit]").removeAttr('disabled');
        actualizarTL();
        $("#add-seguimiento").modal('hide');
    }
})