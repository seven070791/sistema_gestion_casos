const Toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 3000
});

$(document).on('submit', "#login-user", function(e){
	e.preventDefault();
	let datos = {
		"username" : $("#usuario-email").val(),
		"userpass" : $("#usuario-clave").val() 
	}
	$.ajax({
		url:"/signin",
		method:"POST",
		dataType:"JSON",
		data: {
			"data":btoa(JSON.stringify(datos))
		},
		beforeSend:function(){
			$("button[type=submit]").attr('disabled', 'true');
		}
	}).then(function(response){
		Toast.fire({
        type: 'success',
        title: response.message
      });
		$("button[type=submit]").removeAttr('disabled');
		window.location = "/inicio";
	}).catch(function(request){
		switch(request.responseJSON.code)
		{
			case 500:
				Toast.fire({
					type: 'error',
					title: "La Base de datos no se encuentra disponible"
				  });
				break;
			default:
				Toast.fire({
					type: 'error',
					title: "Usuario o contraseña incorrectos"
				  });
				break;

		}
		console.log(request.responseJSON.code);
		$("button[type=submit]").removeAttr('disabled');
	})
});

/*Verficacion de datos en el form*/
$(document).on('change', '#usuario-email',function(e){
	let texto = $("#usuario-email").val();
	if(texto.match(/\w*.\w*\@sapi.gob.ve/) == null){
		$("#usuario-email").addClass('is-invalid');
		$("button[type=submit]").attr('disabled', 'true');
	}
	else if(texto.lenght < 5){
		$("#usuario-email").addClass('is-invalid');
		$("button[type=submit]").attr('disabled', 'true');
	}
	else{
		$("#usuario-email").removeClass('is-invalid');
		$("#usuario-email").addClass('is-valid');
		$("button[type=submit]").removeAttr('disabled');	
	}
});