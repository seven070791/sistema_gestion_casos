//Funcion para actualizar la tabla
function actualizarTabla(){
  $.ajax({
    url:"/actualizarTablaUsuarios",
    method:"POST",
    dataType:"JSON"
  }).then((response) => {
    $("#tabla").html(atob(response.data));
  }).catch((request) => {
    Swal.fire("Error", "Hubo un error al cargar los usuarios", "error");
  });
}

//Verificacion si las dos contraseñas son iguales
$(document).on('keyup', "#user-confirm-pass", (e) => {
  e.preventDefault();
  let pass = $("#user-confirm-pass").val();
  if($("#user-pass").val() != pass){
    $("#user-pass").addClass('is-invalid');
    $("#user-confirm-pass").addClass('is-invalid');
    $("button[type=submit]").attr('disabled', 'true');
  }
  else{
    $("#user-pass").removeClass('is-invalid');
    $("#user-confirm-pass").removeClass('is-invalid');
    $("#user-pass").addClass('is-valid');
    $("#user-confirm-pass").addClass('is-valid');
    $("button[type=submit]").removeAttr('disabled');
  }
});

//Guardado de los datos
$(document).on('submit', "#new-user", function(e){
  e.preventDefault();
  let datos = {
    "username": $("#user-name").val(),
    "userlastname": $("#user-lastname").val(),
    "useremail":$("#user-email").val(),
    "userrol":$("#user-rol").val(),
    "userpass":$("#user-pass").val()
  }
  $.ajax({
    url:"/addNewUser",
    method:"POST",
    dataType:"JSON",
    data:{
      "data" : btoa(JSON.stringify(datos))
    },
    beforeSend:function(){
      $("button[type=submit]").attr('disabled',"true");
    }
  }).then((response) =>{
    Swal.fire('Exito!', "Usuario Registrado","success");
    $("#addUser").modal('hide');
    $("button[type=submit]").removeAttr('disabled');
  }).catch((request) =>{
    Swal.fire("Error!", "Ha ocurrido un error", "error");
    $("button[type=submit]").removeAttr('disabled');
  });
});

//Evento para editar un usuario
$(document).on('click', ".editar", function(e){
  e.preventDefault();
  let id = $(this).attr('id');
  $.ajax({
    url:"/getUser",
    method:"POST",
    dataType:"JSON",
    data: {
      "data":btoa(JSON.stringify({"userid":id}))
    }
  }).then((response) => {
    $("#edit-user-name").val(response.data.name);
    $("#edit-user-lastname").val(response.data.lastname);
    $("#edit-user-email").val(response.data.email);
    $("#edit-user-rol").val(response.data.rol);
    $("#userid").val(response.data.iduser);
    $("#editUser").modal('show');
  }).catch((request) => {
    Swal.fire('Error!', "Ha ocurrido un error", "error");
  });
});

//Evento para guardar el usuario editado
$(document).on('submit', "#edit-user", function(e){
  e.preventDefault();
  let datos = {
    "username": $("#edit-user-name").val(),
    "userlastname": $("#edit-user-lastname").val(),
    "useremail":$("#edit-user-email").val(),
    "userrol":$("#edit-user-rol").val(),
    "userid" : $("#userid").val(),
  }
  $.ajax({
    url:"/editUser",
    method:"POST",
    dataType:"JSON",
    data:{
      "data" : btoa(JSON.stringify(datos))
    },
    beforeSend:function(){
      $("button[type=submit]").attr('disabled',"true");
    }
  }).then((response) =>{
    Swal.fire('Exito!', "Usuario editado exitosamente","success");
    $("#editUser").modal('hide');
    $("button[type=submit]").removeAttr('disabled');
    actualizarTabla();
  }).catch((request) =>{
    Swal.fire("Error!", "Ha ocurrido un error", "error");
    $("button[type=submit]").removeAttr('disabled');
    actualizarTabla();
  });
})